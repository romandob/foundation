<body>
<div class="contain-to-grid" id="top-top-menu">
    <nav class="top-bar" data-topbar role="navigation"  data-options="is_hover: false">
        <section class="top-bar-section">
            <ul class="title-area" id="logo-top">
                <li><span><a href="index.php" class="logo-title"><span class="logo-text">G-Global</span></a></span>
                <span class="headerDivider"></span></li>
                <li class="toggle-topbar menu-icon"><a href=""><span>Menu</span></a></li>

            </ul>
            <ul class="large-12 medium-4">
                <!-- Search | has-form wrapper -->
                <li class="has-form" id="search-form">
                    <div class="large-1 small-1 columns">
                        <a href="#" class="button expand"><span class="icon icon-search-gg"></span></a>
                    </div>
                    <div class="large-10 small-6 columns left">
                        <input type="text" id="right-label" placeholder="Search">
                    </div>
                </li>
            </ul>
            <!-- Right Nav Section -->
            <ul class="right" id="right-top-menu">
             <li class="hide-for-medium-up">
                    <select class="select-lang" id="select-lang-resp">
                        <option value="1" selected="selected" data-imagesrc="img/eng_flag.png" data-description="English language">
                            English
                        </option>
                        <option value="2" data-imagesrc="img/eng_flag.png" data-description="Русский язык">Russian
                        </option>
                        <option value="3" data-imagesrc="img/eng_flag.png" data-description="Eesti keel">Estonian
                        </option>
                    </select>
                </li>
                <li class="show-for-medium-up"><a href="#">About</a></li>
                <li class="show-for-medium-up"><a href="#">Blog</a></li>
                <li class="show-for-medium-up"><a href="#">Success stories</a></li>
                <li class="show-for-medium-up"><a href="#">Contact us</a></li>
                <li class="show-for-medium-up">
                    <select class="select-lang" id="select-lang-main">
                        <option value="1" selected="selected" data-imagesrc="img/eng_flag.png" data-description="English language">
                            English
                        </option>
                        <option value="2" data-imagesrc="img/eng_flag.png" data-description="Русский язык">Russian
                        </option>
                        <option value="3" data-imagesrc="img/eng_flag.png" data-description="Eesti keel">Estonian
                        </option>
                    </select>
                </li>

                <li class="has-dropdown logged hide-for-small-only">
                    <a href="#" class="m-title"><span class="icon icon-profile-gg" id="login-select"></span>Username</a>
                    <ul class="dropdown">
                        <li><a href="account.php"><span class="icon icon-profile-gg"></span>My profile</a></li>
                        <li><a href="account.php"><span class="icon icon-industry-gg"></span>Projects</a></li>
                        <li><a href="account.php"><span class="icon icon-industry-gg"></span>Company</a></li>
                        <li><a href="#"><span class="icon icon-exit-gg"></span>Logout</a></li>
                    </ul>
                </li>
            </ul>
           <ul class="right" id="right-right-top-menu">
                <li class="hide-for-medium-up"><a href="#">About</a></li>
                <li class="hide-for-medium-up"><a href="#">Blog</a></li>
                <li class="hide-for-medium-up"><a href="#">Contact us</a></li>
                <li class="hide-for-medium-up"><a href="projects.php">Explore Projects</a></li>
                <li class="hide-for-medium-up"><a href="companies.php">Explore Companies</a></li>
                <li class="hide-for-medium-up"><a href="project_overview.php">Business for sale</a></li>
            </ul>
            <ul class="right">
                <li class="hide-for-medium-up icon-resp">
                <a href="#" style="padding-right: 20px;margin-top:-2px;"><span class="icon icon-favorite-gg"></span></a>
                <a href="#" style="padding-right: 20px"><span class="icon icon-chat-gg"></span></a>
                <a href="#" style="padding-right: 0"><span class="icon icon-profile-gg"></span><span class="success label round hide-for-small-only" id="count-persons">1</span></a>
                </li>

            </ul>
        </section>
    </nav>
</div>
<!--Stick menu only large and medium-->
<div class="contain-to-grid sticky show-for-medium-up" id="sticky-top-menu">
    <nav class="top-bar"  data-topbar role="navigation" data-options="sticky_on: large">
        <!-- Top Bar Section -->
        <section class="top-bar-section">
            <!-- Top Bar Left Nav Elements -->
            <ul class="title-area" id="logo-top-toggle">
                <li><span><a href="index.php"><span class="icon icon-logo-gg"></span></a></span></li>
            </ul>
            <ul class="left">
                <li><a href="projects.php">Explore Projects</a></li>
                <li ><a href="companies.php">Explore Companies</a></li>
                <li ><a href="project_overview.php">Business for sale</a></li>
            </ul>
            <!-- Top Bar Right Nav Elements -->
            <ul class="right">
                <li><a href="#" style="padding-right: 20px;margin-top:-2px;"><span class="icon icon-favorite-gg"></span></a></li>
                <li><a href="#" style="padding-right: 20px"><span class="icon icon-chat-gg"></span></a></li>
                <li><a href="#" style="padding-right: 0"><span class="icon icon-profile-gg"></span><span class="success label round" id="count-persons">1</span></a></li>
            </ul>
        </section>
    </nav>
</div>
<!--END TOP-->