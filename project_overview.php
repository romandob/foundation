<?php include('head.php');?>
<?php include('top_logged.php');?>

<div class="dark-body top">
    <div class="row collpase">
        <div data-alert class="alert-box success">
            This page was viewed 153 times.
            <a class="edit" href="#"><span class="icon icon-edit-gg"></span> Edit project page&nbsp;&nbsp;|</a><a href="#" class="close">&times;</a>
        </div>
        <h2 class="subheader current">Project name with longname and two rows</h2>
    </div>
 <div class="row collapse">

     <dl class="tabs bordered" data-tab>
         <dd class="active"><a href="#tab-overview">Overview</a></dd>
         <dd><a href="#tab-documentation">Documentation</a></dd>
         <dd><a href="#tab-team">Team</a></dd>
         <dd><a href="#tab-updates">Updates</a></dd>
         <dd style="border-right: 1px solid #ddd;"><a href="#tab-faq">FAQ</a></dd>

     </dl>
 </div>
    <div class="tabs-content">
        <div class="content active" id="tab-overview">
            <div class="row collapse">
               <div class="large-8 medium-6" style="padding-bottom: 20px;">
                   <img src="img/project_overview@2x.jpg" alt="overview" width="592" height="310"/>
               </div>
                <div class="large-4 columns">
                    <div class="label-name-gallery">Images</div>
                    <ul class="small-block-grid-30">
                    <li><a href="#"><img src="img/more_image@2x.jpg" alt="overview" width="69" height="47"/></a></li>
                    <li><a href="#"><img src="img/more_image@2x.jpg" alt="overview" width="69" height="47"/></a></li>
                    <li><a href="#"><img src="img/more_image@2x.jpg" alt="overview" width="69" height="47"/></a></li>
                </ul>
                </div>

              <div class="large-8">
                  <div class="label-name-gallery" style="left: 9px; position: relative;">Videos</div>
                  <ul class="small-block-grid-30">
                      <li><a href="#"><img src="img/more_video@2x.jpg" alt="overview" width="69" height="47"/></a></li>
                      <li><a href="#"><img src="img/more_video@2x.jpg" alt="overview" width="69" height="47"/></a></li>
                  </ul>
              </div>


                <div class="large-8 medium-7">
                    <h3>About project</h3>
                    <p class="events">
                        The Eiffel Tower is an iron lattice tower located on the Champ de <a href="#">Mars in Paris</a>. It was named after the engineer Gustave Eiffel...The Eiffel Tower is an iron lattice tower located on the Champ de Mars in Paris. It was named after the engineer Gustave Eiffel...
                    </p>
                    <p>
                        The Eiffel Tower is an iron lattice tower located on the Champ de Mars in Paris. It was named after the engineer Gustave Eiffel...
                    </p>
                </div>
            </div>

            <div class="hide-for-small-only float-panel">
                <div class="row">
                    <div class="large-6 columns text-left label-name">Investment volume</div>
                    <div class="large-6 columns text-right label-title">$150’000 <span class="label-title-alt">$110’000</span></div>
                </div>
                <div class="row">
                    <div class="large-6 columns text-left label-name">ROI</div>
                    <div class="large-6 columns text-right label-title">7%</div>
                </div>
                <div class="row">
                    <div class="large-6 columns text-left label-name">Investment type</div>
                    <div class="large-6 columns text-right label-title">Shares</div>
                </div>
                <div class="row">
                    <div class="large-6 columns text-left label-name">Country</div>
                    <div class="large-6 columns text-right label-title">Estonia</div>
                </div>
                <div class="row">
                    <div class="large-6 columns text-left label-name">Industry</div>
                    <div class="large-6 columns text-right label-title">infotehnology</div>
                </div>
                <div class="row">
                    <div class="large-6 columns text-left label-name">Project phase</div>
                    <div class="large-6 columns text-right"><span class="label-title">Realization</span>
                        <div class="progress success">
                            <span class="meter" style="width: 70%"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="large-6 columns text-left label-name">Company/Person</div>
                    <div class="large-6 columns text-right label-title">Business Solutions</div>
                </div>
                <div class="large-12 columns large-centered"><a href="#" class="button expand small round success">Send message</a></div>
                <div class="row large-6 large-centered columns add-to-list"><a href="#"><span class="icon icon-favorite-gg"></span> Add to list</a></div>
            </div>
        </div>
        <div class="content" id="tab-documentation">
            <div class="row collapse">

                <div class="large-8 medium-7">
                    <h3>Section title</h3>
                    <p class="events">
                        The Eiffel Tower is an iron lattice tower located on the Champ de Mars in Paris. It was named after the engineer Gustave Eiffel.
                    </p>
                    <table class="list" style="width:100%;">
                        <tbody>
                        <tr>
                            <td style="width: 55%"><img src="img/doc_icon@2x.png" alt="doc" width="27" height="34"/>File_name_and_extention.pdf</td>
                            <td style="width: 15%">2.3 MB</td>
                            <td style="width: 30%"><a href="#" class="button round button-white-gg"><i class="fa fa-arrow-down"></i> Download file</a></td>
                        </tr>
                        <tr>
                            <td style="width: 55%"><img src="img/pdf_icon@2x.png" alt="doc" width="27" height="34"/>File_name_and_extention.docx</td>
                            <td style="width: 15%">17.3 MB</td>
                            <td style="width: 30%"><a href="#" class="button round button-white-gg"><i class="fa fa-arrow-down"></i> Download file</a></td>
                        </tr>
                        <tr>
                            <td style="width: 55%"><img src="img/doc_icon@2x.png" alt="doc" width="27" height="34"/>File_name_and_extention.pdf</td>
                            <td style="width: 15%">2.3 MB</td>
                            <td style="width: 30%"><a href="#" class="button round button-white-gg"><i class="fa fa-arrow-down"></i> Download file</a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="large-8 medium-7">
                    <h3>Section title</h3>
                    <p class="events">
                        The Eiffel Tower is an iron lattice tower located on the Champ de Mars in Paris. It was named after the engineer Gustave Eiffel.
                    </p>
                    <table class="list" style="width:100%;">
                        <tbody>
                        <tr>
                            <td style="width: 55%"><img src="img/doc_icon@2x.png" alt="doc" width="27" height="34"/>File_name_and_extention.pdf</td>
                            <td style="width: 15%">2.3 MB</td>
                            <td style="width: 30%"><a href="#" class="button round button-white-gg"><i class="fa fa-arrow-down"></i> Download file</a></td>
                        </tr>
                        <tr>
                            <td style="width: 55%"><img src="img/pdf_icon@2x.png" alt="doc" width="27" height="34"/>File_name_and_extention.docx</td>
                            <td style="width: 15%">17.3 MB</td>
                            <td style="width: 30%"><a href="#" class="button round button-white-gg"><i class="fa fa-arrow-down"></i> Download file</a></td>
                        </tr>
                        <tr>
                            <td style="width: 55%"><img src="img/doc_icon@2x.png" alt="doc" width="27" height="34"/>File_name_and_extention.pdf</td>
                            <td style="width: 15%">2.3 MB</td>
                            <td style="width: 30%"><a href="#" class="button round button-white-gg"><i class="fa fa-arrow-down"></i> Download file</a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="hide-for-small-only float-panel">

                <div class="row">
                    <div class="large-6 columns text-left label-name">Investment volume</div>
                    <div class="large-6 columns text-right label-title">$150’000 <span class="label-title-alt">$110’000</span></div>
                </div>
                <div class="row">
                    <div class="large-6 columns text-left label-name">ROI</div>
                    <div class="large-6 columns text-right label-title">7%</div>
                </div>
                <div class="row">
                    <div class="large-6 columns text-left label-name">Investment type</div>
                    <div class="large-6 columns text-right label-title">Shares</div>
                </div>
                <div class="row">
                    <div class="large-6 columns text-left label-name">Country</div>
                    <div class="large-6 columns text-right label-title">Estonia</div>
                </div>
                <div class="row">
                    <div class="large-6 columns text-left label-name">Industry</div>
                    <div class="large-6 columns text-right label-title">infotehnology</div>
                </div>
                <div class="row">
                    <div class="large-6 columns text-left label-name">Project phase</div>
                    <div class="large-6 columns text-right"><span class="label-title">Realization</span>
                        <div class="progress success">
                            <span class="meter" style="width: 70%"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="large-6 columns text-left label-name">Company/Person</div>
                    <div class="large-6 columns text-right label-title">Business Solutions</div>
                </div>
                <div class="large-12 columns large-centered"><a href="#" class="button expand small round success">Send message</a></div>
                <div class="row large-6 large-centered columns add-to-list"><a href="#"><span class="icon icon-favorite-gg"></span> Add to list</a></div>
            </div>
        </div>
        <div class="content" id="tab-team">
            <div class="row collapse">

                <div class="row">
                    <div class="large-2 small-12 columns">
                        <div class="box">  <img src="img/ph_img@2x.jpg" alt="photo" height="100"/></div>
                        <span class="small-social-link"><a href="#"><i class="fa fa-facebook-square"></i></a><a href="#"><i class="fa fa-linkedin-square"></i></a></span>
                    </div>
                    <div class="large-6 medium-6 small-12 columns left">
                        <h3>John Appleseed</h3>
                        <div class="sub-h3">Specialisation or position here</div>
                        <a href="mailto:john.appleseed@companyname.com">john.appleseed@companyname.com</a>
                        <p class="events">
                            The Eiffel Tower is an iron lattice tower located on the Champ de Mars in Paris. It was named
                            after the engineer Gustave Eiffel...The Eiffel Tower is an iron lattice tower located on the
                            Champ de Mars in Paris. It was named after the engineer Gustave Eiffel...
                            The Eiffel Tower is an iron lattice tower located on the Champ de Mars in Paris. It was named
                            after the engineer Gustave Eiffel...
                        </p>
                    </div>
                </div>

                <div class="row">
                    <div class="large-2 small-12 columns">
                        <div class="box">  <img src="img/ph_img@2x.jpg" alt="photo" height="100"/></div>
                        <span class="small-social-link"><a href="#"><i class="fa fa-facebook-square"></i></a><a href="#"><i class="fa fa-linkedin-square"></i></a></span>
                    </div>
                    <div class="large-6 medium-6 small-12 columns left">
                        <h3>John Appleseed</h3>
                        <div class="sub-h3">Specialisation or position here</div>
                        <a href="mailto:john.appleseed@companyname.com">john.appleseed@companyname.com</a>
                        <p class="events">
                            The Eiffel Tower is an iron lattice tower located on the Champ de Mars in Paris. It was named
                            after the engineer Gustave Eiffel...The Eiffel Tower is an iron lattice tower located on the
                            Champ de Mars in Paris. It was named after the engineer Gustave Eiffel...
                            The Eiffel Tower is an iron lattice tower located on the Champ de Mars in Paris. It was named
                            after the engineer Gustave Eiffel...
                        </p>
                    </div>
                </div>

            </div>

            <div class="hide-for-small-only float-panel">

                <div class="row">
                    <div class="large-6 columns text-left label-name">Investment volume</div>
                    <div class="large-6 columns text-right label-title">$150’000 <span class="label-title-alt">$110’000</span></div>
                </div>
                <div class="row">
                    <div class="large-6 columns text-left label-name">ROI</div>
                    <div class="large-6 columns text-right label-title">7%</div>
                </div>
                <div class="row">
                    <div class="large-6 columns text-left label-name">Investment type</div>
                    <div class="large-6 columns text-right label-title">Shares</div>
                </div>
                <div class="row">
                    <div class="large-6 columns text-left label-name">Country</div>
                    <div class="large-6 columns text-right label-title">Estonia</div>
                </div>
                <div class="row">
                    <div class="large-6 columns text-left label-name">Industry</div>
                    <div class="large-6 columns text-right label-title">infotehnology</div>
                </div>
                <div class="row">
                    <div class="large-6 columns text-left label-name">Project phase</div>
                    <div class="large-6 columns text-right"><span class="label-title">Realization</span>
                        <div class="progress success">
                            <span class="meter" style="width: 70%"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="large-6 columns text-left label-name">Company/Person</div>
                    <div class="large-6 columns text-right label-title">Business Solutions</div>
                </div>
                <div class="large-12 columns large-centered"><a href="#" class="button expand small round success">Send message</a></div>
                <div class="row large-6 large-centered columns add-to-list"><a href="#"><span class="icon icon-favorite-gg"></span> Add to list</a></div>
            </div>
        </div>
        <div class="content" id="tab-updates">
            <div class="row collapse">
                <div class="large-2 columns"><p class="event">12/01/2015 15:34</p></div>
                <div class="large-5 medium-5 columns">
                    <p class="grey">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vulputate mattis dolor in accumsan.
                        Suspendisse tincidunt pellentesque metus, non accumsan nibh semper quis</p>
                </div>
                <div class="large-12 columns left">
                    <hr/>
                </div>
                <div class="large-2 columns"><p class="event">12/01/2015 15:34</p></div>
                <div class="large-5 medium-5 columns">
                    <p class="grey">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vulputate mattis dolor in accumsan.
                        Suspendisse tincidunt pellentesque metus, non accumsan nibh semper quis</p>
                </div>
                <div class="large-12 columns left">
                    <hr/>
                </div>
                <div class="large-2 columns"><p class="event">12/01/2015 15:34</p></div>
                <div class="large-5 medium-5 columns">
                    <p class="grey">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vulputate mattis dolor in accumsan.
                        Suspendisse tincidunt pellentesque metus, non accumsan nibh semper quis</p>
                </div>
                <div class="large-12 columns left">
                    <hr/>
                </div>
                <div class="large-2 columns"><p class="event">12/01/2015 15:34</p></div>
                <div class="large-5 medium-5 columns">
                    <p class="grey">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vulputate mattis dolor in accumsan.
                        Suspendisse tincidunt pellentesque metus, non accumsan nibh semper quis</p>
                </div>
                <div class="large-12 columns left">
                    <hr/>
                </div>
                <div class="large-2 columns"><p class="event">12/01/2015 15:34</p></div>
                <div class="large-5 medium-5 columns">
                    <p class="grey">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vulputate mattis dolor in accumsan.
                        Suspendisse tincidunt pellentesque metus, non accumsan nibh semper quis</p>
                </div>
                <div class="large-12 columns left">
                    <hr/>
                </div>
                <div class="large-2 columns"><p class="event">12/01/2015 15:34</p></div>
                <div class="large-5 medium-5 columns">
                    <p class="grey">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vulputate mattis dolor in accumsan.
                        Suspendisse tincidunt pellentesque metus, non accumsan nibh semper quis</p>
                </div>
                <div class="large-12 columns left">
                    <hr/>
                </div>
            </div>

            <div class="hide-for-small-only float-panel">

                <div class="row">
                    <div class="large-6 columns text-left label-name">Investment volume</div>
                    <div class="large-6 columns text-right label-title">$150’000 <span class="label-title-alt">$110’000</span></div>
                </div>
                <div class="row">
                    <div class="large-6 columns text-left label-name">ROI</div>
                    <div class="large-6 columns text-right label-title">7%</div>
                </div>
                <div class="row">
                    <div class="large-6 columns text-left label-name">Investment type</div>
                    <div class="large-6 columns text-right label-title">Shares</div>
                </div>
                <div class="row">
                    <div class="large-6 columns text-left label-name">Country</div>
                    <div class="large-6 columns text-right label-title">Estonia</div>
                </div>
                <div class="row">
                    <div class="large-6 columns text-left label-name">Industry</div>
                    <div class="large-6 columns text-right label-title">infotehnology</div>
                </div>
                <div class="row">
                    <div class="large-6 columns text-left label-name">Project phase</div>
                    <div class="large-6 columns text-right"><span class="label-title">Realization</span>
                        <div class="progress success">
                            <span class="meter" style="width: 70%"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="large-6 columns text-left label-name">Company/Person</div>
                    <div class="large-6 columns text-right label-title">Business Solutions</div>
                </div>
                <div class="large-12 columns large-centered"><a href="#" class="button expand small round success">Send message</a></div>
                <div class="row large-6 large-centered columns add-to-list"><a href="#"><span class="icon icon-favorite-gg"></span> Add to list</a></div>
            </div>
        </div>
        <div class="content" id="tab-faq">
            <div class="row collapse">
                <div class="large-7 medium-6 columns">
                    <h3>Question title here</h3>
                    <p class="events">
                        The Eiffel Tower is an iron lattice tower located on the Champ de Mars in Paris. It was named after the engineer Gustave Eiffel...The
                        <a href="#">Eiffel Tower</a> is an iron lattice tower located on the Champ de Mars in Paris. It was named after the engineer Gustave Eiffel...
                        The Eiffel Tower is an iron lattice tower located on the Champ de Mars in Paris. It was named after the engineer Gustave Eiffel...
                    </p>
                </div>
                <div class="large-5 columns"></div>

                <div class="large-7 medium-6 columns">
                    <h3>Question title here</h3>
                    <p class="events">
                        The Eiffel Tower is an iron lattice tower located on the Champ de Mars in Paris. It was named after the engineer Gustave Eiffel...The
                        <a href="#">Eiffel Tower</a> is an iron lattice tower located on the Champ de Mars in Paris. It was named after the engineer Gustave Eiffel...
                        The Eiffel Tower is an iron lattice tower located on the Champ de Mars in Paris. It was named after the engineer Gustave Eiffel...
                    </p>
                </div>
                <div class="large-5 medium-5 columns"></div>
                <div class="large-7 columns">
                    <h3>Question title here</h3>
                    <p class="events">
                        The Eiffel Tower is an iron lattice tower located on the Champ de Mars in Paris. It was named after the engineer Gustave Eiffel...The
                        <a href="#">Eiffel Tower</a> is an iron lattice tower located on the Champ de Mars in Paris. It was named after the engineer Gustave Eiffel...
                        The Eiffel Tower is an iron lattice tower located on the Champ de Mars in Paris. It was named after the engineer Gustave Eiffel...
                    </p>
                </div>
                <div class="large-5 columns"></div>
                <div class="large-7 columns">
                    <h3>Question title here</h3>
                    <p class="events">
                        The Eiffel Tower is an iron lattice tower located on the Champ de Mars in Paris. It was named after the engineer Gustave Eiffel...The
                        <a href="#">Eiffel Tower</a> is an iron lattice tower located on the Champ de Mars in Paris. It was named after the engineer Gustave Eiffel...
                        The Eiffel Tower is an iron lattice tower located on the Champ de Mars in Paris. It was named after the engineer Gustave Eiffel...
                    </p>
                </div>
                <div class="large-5 columns"></div>

            </div>

            <div class="hide-for-small-only float-panel">

                <div class="row">
                    <div class="large-6 columns text-left label-name">Investment volume</div>
                    <div class="large-6 columns text-right label-title">$150’000 <span class="label-title-alt">$110’000</span></div>
                </div>
                <div class="row">
                    <div class="large-6 columns text-left label-name">ROI</div>
                    <div class="large-6 columns text-right label-title">7%</div>
                </div>
                <div class="row">
                    <div class="large-6 columns text-left label-name">Investment type</div>
                    <div class="large-6 columns text-right label-title">Shares</div>
                </div>
                <div class="row">
                    <div class="large-6 columns text-left label-name">Country</div>
                    <div class="large-6 columns text-right label-title">Estonia</div>
                </div>
                <div class="row">
                    <div class="large-6 columns text-left label-name">Industry</div>
                    <div class="large-6 columns text-right label-title">infotehnology</div>
                </div>
                <div class="row">
                    <div class="large-6 columns text-left label-name">Project phase</div>
                    <div class="large-6 columns text-right"><span class="label-title">Realization</span>
                        <div class="progress success">
                            <span class="meter" style="width: 70%"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="large-6 columns text-left label-name">Company/Person</div>
                    <div class="large-6 columns text-right label-title">Business Solutions</div>
                </div>
                    <div class="large-12 columns large-centered"><a href="#" class="button expand small round success">Send message</a></div>
                <div class="row large-6 large-centered columns add-to-list"><a href="#"><span class="icon icon-favorite-gg"></span> Add to list</a></div>
            </div>

        </div>

    </div>
</div>

<?php include('footer.php');?>


