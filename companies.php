<?php include('head.php');?>
<?php include('top.php');?>

<div class="dark-body top">
    <div class="row collpase">
        <h2 class="subheader">Companies</h2>
    </div>
 <div class="row collapse">
     <dl class="tabs" data-tab>
         <dd><a href="#panel1">All companies</a></dd>
         <dd class="active"><a href="#panel2">Recommended</a></dd>

     </dl>
 </div>
    <div class="tabs-content">
        <div class="content" id="panel1">
          <div class="row collapse">
              <h3 class="left">All companies</h3>
          </div>
        </div>
        <div class="content active" id="panel2">
            <div class="row collapse section">
                <div class="large-3 medium-4 columns">
                    <label for="select-countries show-for-medium-up">Countries</label>
                    <section id="select-country">
                        <select class="cs-select cs-skin-border" id="select-countries">
                            <option value="" disabled selected>Countries (3)</option>
                            <option value="germany">Germany</option>
                            <option value="uk">United Kindom</option>
                            <option value="sweden">Sweden</option>
                        </select>
                    </section>
                </div>
                <div class="large-3 medium-4 columns">
                    <label for="select-industries show-for-medium-up">Industries</label>
                    <section id="select-industrial">
                        <select class="cs-select cs-skin-border" id="select-industries">
                            <option value="" disabled selected>All Industries</option>
                            <option value="germany">Industrial-1</option>
                            <option value="uk">Industrial-2</option>
                            <option value="sweden">Industrial-3</option>
                            <option value="sweden">Industrial-4</option>
                        </select>
                    </section>
                </div>
                <div class="large-3 medium-3 columns">
                    <label for="select-turnover show-for-medium-up">Turnover</label>
                    <section id="select-range">
                        <select class="cs-select cs-skin-border" id="select-turnover">
                            <option value="" disabled selected>100k - 1min </option>
                            <option value="germany">99999</option>
                            <option value="uk">12121221/option>
                            <option value="sweden">444444</option>
                            <option value="sweden">67238</option>
                        </select>
                    </section>
                </div>
                <div class="large-12 columns">
                    <ul class="button-group">
                        <li><a href="#" class="small button success">Filter</a></li>
                        <li></li>
                        <li><a href="#" class="small button default">More options</a></li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="dark-body">
<div class="row collapse">
    <div class="list-body">
     <div class="row list-row">
         <div class="large-2 columns">
             <img src="img/chevron-Logo.jpg" alt="logo" width="91" height="101"/>
         </div>
         <div class="large-6 columns">
             <h3>Chevron Solutions Ltd.</h3>
           <div>  <span class="label-name">Industry</span><span class="label-title">Energy Environmen</span></div>
            <div> <span class="label-name">Turnover</span><span class="label-title">&#36;15,000,000</span></div>
             <p>The Eiffel Tower is an iron lattice tower located on the Champ de Mars in Paris. It was named after the engineer Gustave Eiffel.</p>
         </div>
         <div class="large-4 columns">
             <div><a href="#" class="icon icon-chat-gg"></a>
                 <a href="#" class="icon icon-favorite-gg active"></a></div>
<div class="sub-address">6 Parvis Notre-Dame - Place Jean-Paul II</br>75004 Paris, France</div>
             <a href="company.php" class="moredetails">More details</a>
         </div>
     </div>
        <hr/>
        <div class="row list-row">
            <div class="large-2 columns">
                <img src="img/chevron-Logo.jpg" alt="logo" width="91" height="101"/>
            </div>
            <div class="large-6 columns">
                <h3>Chevron Solutions Ltd.</h3>
                <div>  <span class="label-name">Industry</span><span class="label-title">Energy Environmen</span></div>
                <div> <span class="label-name">Turnover</span><span class="label-title">&#36;15,000,000</span></div>
                <p>The Eiffel Tower is an iron lattice tower located on the Champ de Mars in Paris. It was named after the engineer Gustave Eiffel.</p>
            </div>
            <div class="large-4 columns">
                <div><a href="#" class="icon icon-chat-gg"></a>
                    <a href="#" class="icon icon-favorite-gg active"></a></div>
                <div class="sub-address">6 Parvis Notre-Dame - Place Jean-Paul II</br>75004 Paris, France</div>
                <a href="company.php" class="moredetails">More details</a>
            </div>
        </div>
        <hr/>
        <div class="row list-row">
            <div class="large-2 columns">
               <div class="img-empty"></div>
            </div>
            <div class="large-6 columns">
                <h3>Chevron Solutions Ltd.</h3>
                <div>  <span class="label-name">Industry</span><span class="label-title">Energy Environmen</span></div>
                <div> <span class="label-name">Turnover</span><span class="label-title">&#36;15,000,000</span></div>
                <p>The Eiffel Tower is an iron lattice tower located on the Champ de Mars in Paris. It was named after the engineer Gustave Eiffel.</p>
            </div>
            <div class="large-4 columns">
                <div><a href="#" class="icon icon-chat-gg"></a>
                    <a href="#" class="icon icon-favorite-gg active"></a></div>
                <div class="sub-address">6 Parvis Notre-Dame - Place Jean-Paul II</br>75004 Paris, France</div>
                <a href="company.php" class="moredetails">More details</a>
            </div>
        </div>

    </div>
</div>
    <div class="row">
        <div class="large-12" style="text-align: center;">
            <a class="showmore" href="#">Show more Companies</a>
        </div>
    </div>
</div>
<?php include('footer.php');?>


