<?php include('head.php');?>
<?php include('top.php');?>

<div class="dark-body top">
    <div class="row collpase">
        <h2 class="subheader">Projects</h2>
    </div>
 <div class="row collapse">
     <div class="large-4 columns right">
         <a class="add" href="#"><span class="icon icon-plus-gg"></span>Add project</a>
     </div>
     <dl class="tabs" data-tab>
         <dd><a href="#panel1">All projects</a></dd>
         <dd class="active"><a href="#panel2">Recommended</a></dd>

     </dl>
 </div>
    <div class="tabs-content">
        <div class="content" id="panel1">
          <div class="row collapse">
              <h3 class="left">All projects</h3>
          </div>
        </div>
        <div class="content active" id="panel2">
            <div class="row collapse section">
                <div class="large-3 medium-4 columns">
                    <label for="select-countries show-for-medium-up">Countries</label>
                    <section id="select-country">
                        <select class="cs-select cs-skin-border" id="select-countries">
                            <option value="" disabled selected>Countries (3)</option>
                            <option value="germany">Germany</option>
                            <option value="uk">United Kindom</option>
                            <option value="sweden">Sweden</option>
                        </select>
                    </section>
                </div>
                <div class="large-3 medium-4 columns">
                    <label for="select-industries show-for-medium-up">Industries</label>
                    <section id="select-industrial">
                        <select class="cs-select cs-skin-border" id="select-industries">
                            <option value="" disabled selected>All Industries</option>
                            <option value="germany">Industrial-1</option>
                            <option value="uk">Industrial-2</option>
                            <option value="sweden">Industrial-3</option>
                            <option value="sweden">Industrial-4</option>
                        </select>
                    </section>
                </div>
                <div class="large-3 medium-3 columns">
                    <label for="select-turnover show-for-medium-up">Turnover</label>
                    <section id="select-range">
                        <select class="cs-select cs-skin-border" id="select-turnover">
                            <option value="" disabled selected>100k - 1min </option>
                            <option value="germany">99999</option>
                            <option value="uk">12121221/option>
                            <option value="sweden">444444</option>
                            <option value="sweden">67238</option>
                        </select>
                    </section>
                </div>
                <div class="large-12 columns">
                    <ul class="button-group">
                        <li><a href="#" class="small button success">Filter</a></li>
                        <li></li>
                        <li><a href="#" class="small button default">More options</a></li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="dark-body">
    <div class="row large-collapse" id="blocks-gg">
        <!--Block-1-gg-->
        <div class="large-4 medium-6 columns">
            <div class="panel radius panel-left">
                <div class="img-wrapper" id="block-gg-img-1"></div>
                <div class="content row">
                    <div class="info"> <span class="left">$10k - $150k</span><span class="right">23 hours left</span></div>
                    <dl>
                        <dt class="large-10">Project name with longname and two rows</dt>
                        <dd class="small">Industry one</dd>
                        <dd>The Eiffel Tower is an iron lattice tower located on the Champ de Mars in Paris It was named after the engineer Gustave Eiffel...</dd>
                        <dd class="map-label"><span class="icon icon-location-gg"></span>California, USA</dd>
                    </dl>
                    <div class="progress success large-6 medium-6 small-6 columns left">
                        <span class="meter" style="width: 70%"></span>
                    </div>
                    <div class="large-6 medium-6 small-6 columns right">
                        <span class="progress-info">Operational</span>
                    </div>
                </div>
                <div class="row buttons">
                    <div class="button large-6 columns left">Read more</div>
                    <div class="button large-6 columns"><span class="icon icon-favorite-gg"></span> Add to list</div>
                </div>
            </div>
        </div>
        <!--Block-2-gg-->
        <div class="large-4 medium-6 columns">
            <div class="panel radius panel-center">
                <div class="img-wrapper" id="block-gg-img-2"></div>
                <div class="content row">
                    <div class="info"> <span class="left">$10k - $150k</span><span class="right">23 hours left</span></div>
                    <dl>
                        <dt class="large-10">Project name with longname and two rows</dt>
                        <dd class="small">Industry one</dd>
                        <dd>The Eiffel Tower is an iron lattice tower located on the Champ de Mars in Paris It was named after the engineer Gustave Eiffel...</dd>
                        <dd class="map-label"><span class="icon icon-location-gg"></span>California, USA</dd>
                    </dl>
                    <div class="progress success large-6 medium-6 small-6 columns left">
                        <span class="meter" style="width: 70%"></span>
                    </div>
                    <div class="large-6 medium-6 small-6 columns right">
                        <span class="progress-info">Operational</span>
                    </div>
                </div>
                <div class="row buttons">
                    <div class="button large-6 columns left">Read more</div>
                    <div class="button large-6 columns"><span class="icon icon-favorite-gg active"></span>&nbsp;Remove from list</div>
                </div>
            </div>
        </div>
        <!--Block-3-gg-->
        <div class="large-4 medium-6 columns">
            <div class="panel radius panel-right">
                <div class="img-wrapper" id="block-gg-img-3"></div>
                <div class="content row">
                    <div class="info"> <span class="left">$10k - $150k</span><span class="right">23 hours left</span></div>
                    <dl>
                        <dt class="large-10">Project name with longname and two rows</dt>
                        <dd class="small">Industry one</dd>
                        <dd>The Eiffel Tower is an iron lattice tower located on the Champ de Mars in Paris It was named after the engineer Gustave Eiffel...</dd>
                        <dd class="map-label"><span class="icon icon-location-gg"></span>California, USA</dd>
                    </dl>
                    <div class="progress success large-6 medium-6 small-6 columns left">
                        <span class="meter" style="width: 70%"></span>
                    </div>
                    <div class="large-6 medium-6 small-6 columns right">
                        <span class="progress-info">Operational</span>
                    </div>
                </div>
                <div class="row buttons">
                    <div class="button large-6 columns left">Read more</div>
                    <div class="button large-6 columns"><span class="icon icon-favorite-gg"></span> Add to list</div>
                </div>
            </div>
        </div>
        <!--Block-4-gg-->
        <div class="large-4 medium-6 columns">
            <div class="panel radius panel-left">
                <div class="img-wrapper" id="block-gg-img-4"></div>
                <div class="content row">
                    <div class="info"> <span class="left">$10k - $150k</span><span class="right">23 hours left</span></div>
                    <dl>
                        <dt class="large-10">Project name with longname and two rows</dt>
                        <dd class="small">Industry one</dd>
                        <dd>The Eiffel Tower is an iron lattice tower located on the Champ de Mars in Paris It was named after the engineer Gustave Eiffel...</dd>
                        <dd class="map-label"><span class="icon icon-location-gg"></span>California, USA</dd>
                    </dl>
                    <div class="progress success large-6 medium-6 small-6 columns left">
                        <span class="meter" style="width: 70%"></span>
                    </div>
                    <div class="large-6 medium-6 small-6 columns right">
                        <span class="progress-info">Operational</span>
                    </div>
                </div>
                <div class="row buttons">
                    <div class="button large-6 columns left">Read more</div>
                    <div class="button large-6 columns"><span class="icon icon-favorite-gg"></span> Add to list</div>
                </div>
            </div>
        </div>
        <!--Block-5-gg-->
        <div class="large-4 medium-6 columns">
            <div class="panel radius panel-center">
                <div class="img-wrapper" id="block-gg-img-5"></div>
                <div class="content row">
                    <div class="info"> <span class="left">$10k - $150k</span><span class="right">23 hours left</span></div>
                    <dl>
                        <dt class="large-10">Project name with longname and two rows</dt>
                        <dd class="small">Industry one</dd>
                        <dd>The Eiffel Tower is an iron lattice tower located on the Champ de Mars in Paris It was named after the engineer Gustave Eiffel...</dd>
                        <dd class="map-label"><span class="icon icon-location-gg"></span>California, USA</dd>
                    </dl>
                    <div class="progress success large-6 medium-6 small-6 columns left">
                        <span class="meter" style="width: 70%"></span>
                    </div>
                    <div class="large-6 medium-6 small-6 columns right">
                        <span class="progress-info">Operational</span>
                    </div>
                </div>
                <div class="row buttons">
                    <div class="button large-6 columns left">Read more</div>
                    <div class="button large-6 columns"><span class="icon icon-favorite-gg"></span> Add to list</div>
                </div>
            </div>
        </div>
        <!--Block-6-gg-->
        <div class="large-4 medium-6 columns">
            <div class="panel radius panel-right">
                <div class="img-wrapper" id="block-gg-img-6"></div>
                <div class="content row">
                    <div class="info"> <span class="left">$10k - $150k</span><span class="right">23 hours left</span></div>
                    <dl>
                        <dt class="large-10">Project name with longname and two rows</dt>
                        <dd class="small">Industry one</dd>
                        <dd>The Eiffel Tower is an iron lattice tower located on the Champ de Mars in Paris It was named after the engineer Gustave Eiffel...</dd>
                        <dd class="map-label"><span class="icon icon-location-gg"></span>California, USA</dd>
                    </dl>
                    <div class="progress success large-8 medium-8 small-8 columns left">
                        <span class="meter" style="width: 40%"></span>
                    </div>
                    <div class="large-4 medium-4 small-4 columns right">
                        <span class="progress-info">Operational</span>
                    </div>
                </div>
                <div class="row buttons">
                    <div class="button large-6 columns left">Read more</div>
                    <div class="button large-6 columns"><span class="icon icon-favorite-gg"></span> Add to list</div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="large-12" style="text-align: center;">
            <a class="showmore" href="#">Show more</a>
        </div>
    </div>
</div>
<?php include('footer.php');?>


