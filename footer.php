<!--end stick menu-->
<div id="footer">
    <div class="overlay">
        <div class="footer-video"></div>
        <div class="hero-bottom">
            <div class="large-7 large-centered medium-10 medium-centered columns">
                <h2>Share ideas worldwide or invest to interesting projects</h2>
            </div>
            <div class="large-3 large-offset-3 medium-4 medium-offset-2 columns">
                <a href="#" class="button radius expand  right">Invest to project</a>
            </div>
            <div class="large-3 large-uncentered medium-4 medium-uncentered columns">
                <a href="#" class="button radius expand success left">Start new project</a>
            </div>
            <!--here is play href-->
        </div>
    </div>
    <div class="footer">
        <div class="row large-collapse">
            <div class="large-8 columns">
                <ul class="inline-list" id="bottom-menu-gg">
                    <li><a href="#">Explore Projects</a></li>
                    <li><a href="#">Explore Companies</a></li>
                    <li><a href="#">About</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Success stories</a></li>
                    <li><a href="#">Contact Us</a></li>
                </ul>
            </div>
            <div class="large-4 medium-12 columns">
                <ul class="no-bullet" id="right-contact-f">
                    <li class="article">Customer Support 24/7</li>
                    <li>+372 52 452 334</li>
                    <li><a href="mailto:support@gglobal.com">support@gglobal.com</a></li>
                </ul>
            </div>
        </div>
        <div class="row large-collapse" id="copyright">
            <div class="large-8 medium-7 columns">
                <ul class="inline-list" id="copyright-gg">
                    <li>©2014 F-Global. All rights reserved.</li>
                    <li><a href="#">About</a></li>
                    <li class="divider-inline"></li>
                    <li><a href="#">Terms and Conditions</a></li>
                    <li class="divider-inline"></li>
                    <li><a href="#">Private Policy</a></li>
                </ul>
            </div>
            <div class="large-4 medium-5 columns">
                <ul class="inline-list no-bullet" id="social-links">
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                    <li><a href="#"><i class="fa fa-rss"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/foundation/js/foundation.min.js"></script>
<script src="js/jquery.ddslick.min.js"></script>
<script src="js/classie.js"></script>
<script src="js/selectFx.js"></script>
<script src="js/jquery.multiselect.js"></script>
<script src="js/responsive-tables.js"></script>
<script src="js/app.js"></script>
</body>
</html>