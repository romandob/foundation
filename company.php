<?php include('head.php');?>
<?php include('top.php');?>



<div class="row collapse" style="padding-top: 40px">

    <div class="large-8 columns">

    </div>
    <div class="large-4 columns">
        <div class="list-body sidebar">
    <div class="logo">
        <img src="img/chevron-Logo.jpg" alt="chevron" width="38%" style="text-align: center;"/>
    </div>
            <span class="secondary label">Business information</span>
            <div class="row collapse">
                <div class="large-6 columns text-left label-name">Annual <br/>Turnover</div>
                <div class="large-6 columns text-right label-title">$150’000 <span class="label-title-alt">$110’000</span></div>
            </div>
            <div class="row collapse">
                <div class="large-6 columns text-left label-name">Industry</div>
                <div class="large-6 columns text-right label-title">Infotehnologiy</div>
            </div>
            <div class="row collapse">
                <div class="large-6 columns text-left label-name">Company type</div>
                <div class="large-6 columns text-right label-title">Limited liability company</div>
            </div>
            <div class="row collapse">
                <div class="large-6 columns text-left label-name">Employees</div>
                <div class="large-6 columns text-right label-title">500-1500</div>
            </div>
            <span class="secondary label">Projects by company</span>
        </div>
    </div>

</div>
<?php include('footer.php');?>


