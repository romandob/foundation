<?php include('head.php');?>
<?php include('top.php');?>
<div class="overlay">
    <div class="row">
      <div class="hero-container">
          <div class="large-12 columns">
      <div class="large-8 medium-10 medium-centered large-centered columns">
          <h2>Share ideas worldwide or invest to interesting projects</h2>
      </div>
              <div class="large-3 large-offset-3 medium-4 medium-offset-2 columns">
                  <a href="#" class="button radius expand  right">Invest to project</a>
              </div>
              <div class="large-3 large-uncentered medium-4 medium-uncentered columns">
                  <a href="#" class="button radius expand success left">Start new project</a>
              </div>
              <!--here is play href-->
          </div>
          <div class="large-12 columns" id="play-title">
              <a href="#"><span class="icon icon-play-gg"></span></a><span>watch videos</span>
          </div>
      </div>
      <!-- test -->
    </div>
    <div class="hero-video">
    </div>
</div>
<div class="row large-collapse header-section-gg">
    <div class="large-12">
        <h3 class="left">Most popluar industries</h3>
        <a href="#" class="readmore right">View all industries</a>
    </div>
</div>

<div class="row large-collapse" id="content-menu-gg">
    <div class="large-4 medium-6 columns">
        <a class="button radius button-default-gg" href="">Industry one</a>
    </div>
    <div class="large-4 medium-6 columns" style="text-align: center;">
        <a class="button radius button-default-gg" href="">Industry five</a>
    </div>
    <div class="large-4 medium-6 columns">
        <a class="button radius right button-default-gg" href="">Industry thirty nine</a>
    </div>
    <div class="large-4 medium-6 columns">
        <a class="button radius button-default-gg" href="">Industry three</a>
    </div>
    <div class="large-4 medium-6 columns" style="text-align: center;">
        <a class="button radius button-default-gg" href="">Industry twenty two</a>
    </div>
    <div class="large-4 medium-6 columns">
        <a class="button radius right button-default-gg" href="">Industry seven</a>
    </div>
</div>

<div class="dark-body">
    <div class="row collapse section">
        <div class="large-6 columns">
            <h3 class="left">Recommended projects</h3>
            <span class="right show-for-large-only">Filter by:</span>
        </div>
        <div class="large-3 columns">
            <section id="select-country">
                <select class="cs-select cs-skin-border">
                    <option value="" disabled selected>Countries (3)</option>
                    <option value="germany">Germany</option>
                    <option value="uk">United Kindom</option>
                    <option value="sweden">Sweden</option>
                </select>
            </section>
        </div>
        <div class="large-3 columns">
            <section id="select-industrial">
                <select class="cs-select cs-skin-border">
                    <option value="" disabled selected>All Industries</option>
                    <option value="germany">Industrial-1</option>
                    <option value="uk">Industrial-2</option>
                    <option value="sweden">Industrial-3</option>
                    <option value="sweden">Industrial-4</option>
                </select>
            </section>
        </div>
    </div>
    <div class="row large-collapse" id="blocks-gg">
        <!--Block-1-gg-->
        <div class="large-4 medium-6 columns">
            <div class="panel radius panel-left">
                <div class="img-wrapper" id="block-gg-img-1"></div>
                <div class="content row">
                    <div class="info"> <span class="left">$10k - $150k</span><span class="right">23 hours left</span></div>
                    <dl>
                        <dt class="large-10">Project name with longname and two rows</dt>
                        <dd class="small">Industry one</dd>
                        <dd>The Eiffel Tower is an iron lattice tower located on the Champ de Mars in Paris It was named after the engineer Gustave Eiffel...</dd>
                        <dd class="map-label"><span class="icon icon-location-gg"></span>California, USA</dd>
                    </dl>
                    <div class="progress success large-6 medium-6 small-6 columns left">
                        <span class="meter" style="width: 70%"></span>
                    </div>
                    <div class="large-6 medium-6 small-6 columns right">
                        <span class="progress-info">Operational</span>
                    </div>
                </div>
                <div class="row buttons">
                    <a href="project_overview.php" class="button large-6 columns left">Read more</a>
                    <a class="button large-6 columns"><span class="icon icon-favorite-gg"></span> Add to list</a>
                </div>
            </div>
        </div>
        <!--Block-2-gg-->
        <div class="large-4 medium-6 columns">
            <div class="panel radius panel-center">
                <div class="img-wrapper" id="block-gg-img-2"></div>
                <div class="content row">
                    <div class="info"> <span class="left">$10k - $150k</span><span class="right">23 hours left</span></div>
                    <dl>
                        <dt class="large-10">Project name with longname and two rows</dt>
                        <dd class="small">Industry one</dd>
                        <dd>The Eiffel Tower is an iron lattice tower located on the Champ de Mars in Paris It was named after the engineer Gustave Eiffel...</dd>
                        <dd class="map-label"><span class="icon icon-location-gg"></span>California, USA</dd>
                    </dl>
                    <div class="progress success large-6 medium-6 small-6 columns left">
                        <span class="meter" style="width: 70%"></span>
                    </div>
                    <div class="large-6 medium-6 small-6 columns right">
                        <span class="progress-info">Operational</span>
                    </div>
                </div>
                <div class="row buttons">
                    <a href="project_overview.php" class="button large-6 columns left">Read more</a>
                    <a href="project_overview.php" class="button large-6 columns"><span class="icon icon-favorite-gg active"></span>&nbsp;Remove from list</a>
                </div>
            </div>
        </div>
        <!--Block-3-gg-->
        <div class="large-4 medium-6 columns">
            <div class="panel radius panel-right">
                <div class="img-wrapper" id="block-gg-img-3"></div>
                <div class="content row">
                    <div class="info"> <span class="left">$10k - $150k</span><span class="right">23 hours left</span></div>
                    <dl>
                        <dt class="large-10">Project name with longname and two rows</dt>
                        <dd class="small">Industry one</dd>
                        <dd>The Eiffel Tower is an iron lattice tower located on the Champ de Mars in Paris It was named after the engineer Gustave Eiffel...</dd>
                        <dd class="map-label"><span class="icon icon-location-gg"></span>California, USA</dd>
                    </dl>
                    <div class="progress success large-6 medium-6 small-6 columns left">
                        <span class="meter" style="width: 70%"></span>
                    </div>
                    <div class="large-6 medium-6 small-6 columns right">
                        <span class="progress-info">Operational</span>
                    </div>
                </div>
                <div class="row buttons">
                    <a href="project_overview.php" class="button large-6 columns left">Read more</a>
                    <a class="button large-6 columns"><span class="icon icon-favorite-gg"></span> Add to list</a>
                </div>
            </div>
        </div>
        <!--Block-4-gg-->
        <div class="large-4 medium-6 columns">
            <div class="panel radius panel-left">
                <div class="img-wrapper" id="block-gg-img-4"></div>
                <div class="content row">
                    <div class="info"> <span class="left">$10k - $150k</span><span class="right">23 hours left</span></div>
                    <dl>
                        <dt class="large-10">Project name with longname and two rows</dt>
                        <dd class="small">Industry one</dd>
                        <dd>The Eiffel Tower is an iron lattice tower located on the Champ de Mars in Paris It was named after the engineer Gustave Eiffel...</dd>
                        <dd class="map-label"><span class="icon icon-location-gg"></span>California, USA</dd>
                    </dl>
                    <div class="progress success large-6 medium-6 small-6 columns left">
                        <span class="meter" style="width: 70%"></span>
                    </div>
                    <div class="large-6 medium-6 small-6 columns right">
                        <span class="progress-info">Operational</span>
                    </div>
                </div>
                <div class="row buttons">
                    <a href="project_overview.php" class="button large-6 columns left">Read more</a>
                    <a class="button large-6 columns"><span class="icon icon-favorite-gg"></span> Add to list</a>
                </div>
            </div>
        </div>
        <!--Block-5-gg-->
        <div class="large-4 medium-6 columns">
            <div class="panel radius panel-center">
                <div class="img-wrapper" id="block-gg-img-5"></div>
                <div class="content row">
                    <div class="info"> <span class="left">$10k - $150k</span><span class="right">23 hours left</span></div>
                    <dl>
                        <dt class="large-10">Project name with longname and two rows</dt>
                        <dd class="small">Industry one</dd>
                        <dd>The Eiffel Tower is an iron lattice tower located on the Champ de Mars in Paris It was named after the engineer Gustave Eiffel...</dd>
                        <dd class="map-label"><span class="icon icon-location-gg"></span>California, USA</dd>
                    </dl>
                    <div class="progress success large-6 medium-6 small-6 columns left">
                        <span class="meter" style="width: 70%"></span>
                    </div>
                    <div class="large-6 medium-6 small-6 columns right">
                        <span class="progress-info">Operational</span>
                    </div>
                </div>
                <div class="row buttons">
                    <a href="project_overview.php" class="button large-6 columns left">Read more</a>
                    <a class="button large-6 columns"><span class="icon icon-favorite-gg"></span> Add to list</a>
                </div>
            </div>
        </div>
        <!--Block-6-gg-->
        <div class="large-4 medium-6 columns">
            <div class="panel radius panel-right">
                <div class="img-wrapper" id="block-gg-img-6"></div>
                <div class="content row">
                    <div class="info"> <span class="left">$10k - $150k</span><span class="right">23 hours left</span></div>
                    <dl>
                        <dt class="large-10">Project name with longname and two rows</dt>
                        <dd class="small">Industry one</dd>
                        <dd>The Eiffel Tower is an iron lattice tower located on the Champ de Mars in Paris It was named after the engineer Gustave Eiffel...</dd>
                        <dd class="map-label"><span class="icon icon-location-gg"></span>California, USA</dd>
                    </dl>
                    <div class="progress success large-6 medium-6 small-6 columns left">
                        <span class="meter" style="width: 70%"></span>
                    </div>
                    <div class="large-6 medium-6 small-6 columns right">
                        <span class="progress-info">Operational</span>
                    </div>
                </div>
                <div class="row buttons">
                    <a href="project_overview.php" class="button large-6 columns left">Read more</a>
                    <a class="button large-6 columns"><span class="icon icon-favorite-gg"></span> Add to list</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="large-12" style="text-align: center;">
            <a class="showmore" href="#">Show more recommended projects</a>
        </div>
    </div>
</div>
<?php include('footer.php');?>


